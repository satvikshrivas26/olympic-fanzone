package server

import (
	"gitlab.com/satvikshrivas26/olympic-fanzone/db"
	"github.com/gin-gonic/gin"
	"gitlab.com/satvikshrivas26/olympic-fanzone/controllers"
	_ "github.com/go-sql-driver/mysql"
	
)

func Init() {
	// logger.Init()
	db.Init()
	// es.Init()
	// redis.Init()
	CreateUrlMappings()
	// Listen and server on 0.0.0.0:8080
	router.Run(":8080")
}
